import useAxios from "hooks/useAxios"

const useUsers = () => {
  const axios = useAxios()
  const addUser = async user => {}

  const updateUser = async (id, user) => {}

  const getUsers = async () => {
    try {
      const response = await axios({
        url: "users",
      })
      return response.data
    } catch (error) {
      return error
    }
  }

  const getUserById = async id => {}

  const deleteUser = async id => {}

  return {getUsers, getUserById, addUser, updateUser, deleteUser}
}

export default useUsers
