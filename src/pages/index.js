import {useEffet} from "react"
import useSWR from "swr"
import Head from "next/head"
import Image from "next/image"
import useUsers from "network/users"
import Link from "next/link"
import {useRouter} from "next/router"

const Home = () => {
  const {getUsers} = useUsers()
  const {push} = useRouter()
  const {data, error} = useSWR("users", getUsers)

  const navToUsers = () => push("/users")
  return (
    <div>
      {/* <Link href="/users" passHref>
        <span>Users</span>
      </Link> */}
      <button onClick={navToUsers}>Users</button>
    </div>
  )
}

export default Home
