import {useEffect} from "react"
// import "globals.css"
import {ThemeProvider} from "styled-components"
import {lightTheme, GlobalStyle} from "styles"

const MyApp = ({Component, pageProps}) => {
  useEffect(
    () => document.documentElement.setAttribute("data-theme", "dark"),
    [],
  )

  return (
    <ThemeProvider theme={lightTheme}>
      <GlobalStyle />
      <Component {...pageProps} />
    </ThemeProvider>
  )
}

export default MyApp
