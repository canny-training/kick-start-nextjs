import {useRouter} from "next/router"

const UserDetail = () => {
  const {back, query} = useRouter()
  const {name, id} = query
  return (
    <div>
      <h3>Query 1: {name}</h3>
      <h3>Query 2: {id}</h3>
      <h1>User Detail</h1>
      <h4 onClick={back}>Go back</h4>
    </div>
  )
}

export default UserDetail
