import {useRouter} from "next/router"
const AllPost = () => {
  const {pathname, query} = useRouter()
  const {all} = query

  return (
    <div>
      <h3>{all?.join(", ")}</h3>
      <h4>Path Name: {pathname}</h4>
      {/* <h4>Query: {}</h4> */}
    </div>
  )
}

// AllPost.getInitialProps = async ({query}) => {
//   console.log("query: ", query)
//   return query
// }

export default AllPost
