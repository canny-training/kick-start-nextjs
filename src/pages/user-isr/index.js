import Image from "next/image"

const Blog = ({user}) => {
  console.log(user)
  const {gender, name, location, email, phone, cell, picture} = user
  const {title, first, last} = name
  const {street, city, state, pincode} = location
  const {large} = picture
  return (
    <div>
      <h2>
        {title}. {first} {last}
      </h2>
      <img src={large} alt="" width={200} height={200} />
      {/* <Image src={large} alt="" width={200} height={200} /> */}
      <h3>
        <strong>Gender:</strong> {gender}
      </h3>
      <h3>
        <strong>Phone:</strong> {phone}
      </h3>
      <h3>
        <strong>Cell:</strong> {cell}
      </h3>
      <h4>Address:</h4>
      <label>
        {street.number} {street.name} {city} {state} {pincode}
      </label>
    </div>
  )
}

// This function gets called at build time on server-side.
// It may be called again, on a serverless function, if
// revalidation is enabled and a new request comes in
export async function getStaticProps() {
  const res = await fetch("https://randomuser.me/api/")
  const user = await res.json()

  return {
    props: {
      user: user.results[0],
    },
    // Next.js will attempt to re-generate the page:
    // - When a request comes in
    // - At most once every 10 seconds
    revalidate: 3, // In seconds
  }
}

// This function gets called at build time on server-side.
// It may be called again, on a serverless function, if
// the path has not been generated.
// export async function getStaticPaths() {
//   const res = await fetch("https://.../posts")
//   const posts = await res.json()

//   // Get the paths we want to pre-render based on posts
//   const paths = posts.map(post => ({
//     params: {id: post.id},
//   }))

//   // We'll pre-render only these paths at build time.
//   // { fallback: blocking } will server-render pages
//   // on-demand if the path doesn't exist.
//   return {paths, fallback: "blocking"}
// }

export default Blog
