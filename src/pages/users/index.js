import {useEffet} from "react"
import useSWR from "swr"
import Head from "next/head"
import Image from "next/image"
import useUsers from "network/users"
import {useRouter} from "next/router"

const Home = () => {
  const {getUsers} = useUsers()
  const {push} = useRouter()
  const {data, error} = useSWR("users", getUsers)

  const onClickUser = user => push(`/user/${user.name}/detail/${user.id}`)
  // "user/tamil/detail/1"
  const onClickButton = async () => {
    const response = await fetch("https://api.publicapis.org/entries")
    const data = await response.json()
    console.log(data)
    // fetch("https://api.publicapis.org/entries").then(res => console.log(res))
    console.log("Something else")
  }

  if (!data && !error) <h1>Loading.....</h1>
  else if (error) <h3>{error}</h3>
  const renderUsers = user => {
    const {name, email, id} = user
    return (
      <tr key={id} onClick={onClickUser.bind(null, user)}>
        <td>{name}</td>
        <td>{email}</td>
      </tr>
    )
  }
  return (
    <div>
      <button onClick={onClickButton}>Start Thread</button>
      <h2>Users</h2>
      <table>
        <thead></thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
        </tr>
        <tbody>{data?.map(renderUsers)}</tbody>
      </table>
    </div>
  )
}

export default Home
