import axios from "axios"

const useAxios = function () {
  const instance = axios.create({
    baseURL: process.env.NEXT_PUBLIC_BASE_URL,
  })
  if (instance) {
    instance.interceptors.request.use(
      config => {
        // Do something before request is sent
        return config
      },
      error => {
        return Promise.reject(error)
      },
    )
    instance.interceptors.response.use(
      res => {
        // Do something with response data
        return Promise.resolve(res)
      },
      async err => {
        // Do something with response error
        return Promise.reject(err)
      },
    )
  }
  return instance
}
export default useAxios
