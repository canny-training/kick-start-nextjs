import {createGlobalStyle} from "styled-components"

export const lightTheme = {
  hexQminMaroon: "#901020",
  hexCtaSecondary: "#ad9189",
  hexTextDark: "#ffffff",
  hexTextPrimary: "#3b414f",
  hexTextSecondary: "#313840",
  hexTextHeading: "#979ba2",
  rgbAllergen: "49, 56, 64",
  hexBannerBg: "#f6f4ea",
  hexVeg: "#0b8457",
  hexNonVeg: "#d25c64",
}

export const GlobalStyle = createGlobalStyle`
:root {
  --alpha-zero: 0;
  --alpha-one: 0.1;
  --alpha-two: 0.2;
  --alpha-three: 0.3;
  --alpha-four: 0.4;
  --alpha-five: 0.5;
  --alpha-six: 0.6;
  --alpha-seven: 0.7;
  --alpha-eight: 0.8;
  --alpha-nine: 0.9;
  --alpha-full: 1;

  --hex-qmin-maroon: #901020;
  --hex-cta-secondary: #ad9189;
  --hex-text-dark: #ffffff;
  --hex-text-primary: #3b414f;
  --hex-text-secondary: #313840;
  --hex-text-heading: #979ba2;
  --rgb-allergen: 49, 56, 64;
  --hex-banner-bg: #f6f4ea;
  --hex-veg: #0b8457;
  --hex-non-veg: #d25c64;
}

html.light {
  --hex-qmin-maroon: #901020;
  --hex-cta-secondary: #ad9189;
  --hex-text-dark: #000000;
  --hex-text-primary: #3b414f;
  --hex-text-secondary: #313840;
  --hex-text-heading: #979ba2;
  --rgb-allergen: 49, 56, 64;
  --hex-banner-bg: #f6f4ea;
  --hex-veg: #0b8457;
  --hex-non-veg: #d25c64;
}

@font-face {
  font-display: swap;
}
@font-face {
  font-family: "Proxima Nova Bold";
  src: local("Proxima Nova Bold"), url("/fonts/ProximaNova-Bold.woff2") format("woff2");
}
@font-face {
  font-family: "Proxima Nova Regular";
  src: local("Proxima Nova Regular"), url("/fonts/ProximaNova-Regular.woff2") format("woff2");
}
@font-face {
  font-family: "Calibri";
  src: local("Calibri"), url("/fonts/Calibri.woff2") format("woff2");
}
@font-face {
  font-family: "Brittany Signature";
  src: local("Brittany Signature"), url("/fonts/BrittanySignature.woff2") format("woff2");
}

html,
body {
  padding: 0;
  margin: 0;
  color: var(--hex-text-primary};
  font-family: "Calibri", -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
    Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
  font-weight: 400;
}

h1,
h3,
h4,
h5,
h6,
input,
button,
.bold {
  font-family: "Proxima Nova Bold";
  font-weight: 700;
}

h1 {
  font-size: 2.5rem;
  line-height: 3rem;
}

h2,
.signature {
  font-size: 2.25rem;
  line-height: 3rem;
  font-family: "Brittany Signature";
  font-weight: 400;
}

h3 {
  font-size: 1.5rem;
  line-height: 1.75rem;
}

h5 {
  font-size: 1.125rem;
  line-height: 1.25rem;
}

h6 {
  font-size: 1rem;
  line-height: 1.25rem;
}

a {
  color: inherit;
  text-decoration: none;
}

label,
span,
p,
text,
small,
.regular {
  font-family: "Proxima Nova Regular";
  font-weight: 600;
}

button {
  font-size: 1.125rem;
  line-height: 1.25rem;
  color: var(--hex-text-dark};
  display: block;
  outline: none;
  padding: 1.25rem 0;
  width: 100%;
  border: none;
  background-color: var(--hex-qmin-maroon};
  border-radius: 50%;
}

* {
  margin: 0;
}
`
